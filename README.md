Role Name
=========
ansible_borg_client

This role will setup a borg and a wrapper script borgbackup_runner on a client. 

What is Borg?
BorgBackup is a deduplicating backup program. Optionally, it supports compression and authenticated encryption. See https://borgbackup.readthedocs.io/en/stable/

What is borgbackup_runner?
In short, the borgbackup_runner initiates a backup of user defined local directories. After a successful backup, the borgbackup_runner will submit a user defined post job to the backup repo. Every minute the postjob_runner will check the queue for post job request and execute it. This is useful for testing backups. 

Additionally, borgbackup_runner has the ability to run a custom command prior to running the backup job. This is useful when backing up databases using a 3rd party utility like Holland. See Example playbook below on how to use this.

Requirements
------------
git, awk, and sed must be installed

Role Variables
--------------

```
borg_required_packages:
  yum:
    - borgbackup
borgbackup_runner_log_directory: /var/log/borgbackup # where to store logs
backup_user: borg # the user account borg should run under
borg_repo_path: /backups # where to store client backup repos
borgrunner_cron_daily: 23 # specify what time of day to run a backup
borgrunner_pre_run_command: sudo holland bk # specify a command to run on the client  before a backup job runs
borgbackup_runner_install_path: /usr/local/borgbackup # specify where to install the borg backup runner
borgbackup_runner_config_name: borgbackup_runner_config.yml # the name of the borgbackup runner configuration file
postjob_runner_executer: ansible # specify the type of executer to use for post backup jobs
borg_home_directory: /home/borg # the location of the borgbackup runner's home directory
borg_repo_hostname: backup_repo # the hostname of the borg repo server
borg_client_hostname: client1 # the hostname of the borgbackup client.
install_borg_runner: true # used to disable installation tasks during a restore tasks.
restore_from_backup: false # used to disable a backup job during the initial installation
backup_set: "{{ lookup('env','BACKUP') }}" # used by the borgbackup post job runner
file_to_restore: var/spool/holland/default/newest/backup_data/test.sql.gz # specify a file or directory to restore from backup
restore_path: /tmp/restore # specify where the files should be restored to
postjob_runner_queue_path: /backups/postjobs # specify where to store the post backup job request. 
borg_repo_name: backups # specify the name of the borg repo. 
borg_retention_policy: # specify the job retention policy. 
  keep_hourly: 3
  keep_daily: 7
  keep_weekly: 4
  keep_monthly: 6

ansible_executor: # define a post backup job. This is optional
  playbook_source: git+https://gitlab.com/dreamer-labs/maniac/ansible_test_dr.git # specify the location of the playbook to run after a successful backup
  role_requirements: # specify where to download the roles required by the playbook
    - git+https://gitlab.com/dreamer-labs/maniac/ansible_role_borg_client.git
    - git+https://gitlab.com/dreamer-labs/maniac/ansible_role_mariadb.git
  ansible_username: "{{ backup_user }}"

borg_repo_ssh_priv_key: | # specify the private ssh key to be used by borg
    -----BEGIN OPENSSH PRIVATE KEY-----
    ...
    -----END OPENSSH PRIVATE KEY-----

borg_repo_ssh_pub_key: | # specify the public ssh key to be used by borg
    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCxJ78jgJj/ppmYKLgaCpJiSMz7wJe2c+rAAfBAh8SOfVMC0OaFawp+IWl4CAD2UqPiR+DgYqfW3t65OF2gQhJJ0u2iTsEklpFUKrgbE/B7LZFQ5Q8ZAeET4BnP7R3Aznn2F600Yv2L4xt303/Zx3dLsT3oT6RoUNPA+N4+na8J9PGpHXcqChhFiXalqXpOD0al1dekPr2Q1TRgmDArTiB1P6I8PNqi4VbwlnNCV99qfjsyf2xVTZ12Scf7e4vS2lpiLz46DJx//iZgxGlHLsTZvjISs9yLgkhDYVHzlGrwidSOCSl1LdhZLCajlnEq4kW3IXtaPzAPqUaHxr2nzZPf briguy@example.com

borg_pass: | # specify a passphrase to use to encrypt the borg repo.
    xxxxxxxxxxxxxxx
```

Dependencies
------------

ansible_role_packages
ansible_role_repository

Example Playbook
----------------

```
- hosts: borgclients
  vars:
    borgrunner_pre_run_command: sudo holland bk
  roles:
     -  role: ansible_role_borg_client
```

License
-------

BSD

Author Information
------------------

The Development Range Engineering, Architecture, and Modernization (DREAM) Team
